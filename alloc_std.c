#define _POSIX_C_SOURCE 200809L
#include "alloc.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

static void *std_alloc(struct alloc_context *context, size_t size)
{
	assert(context == NULL);
	(void) context;
	return malloc(size);
}

static void std_dealloc(struct alloc_context *context, void *ptr)
{
	assert(context == NULL);
	(void) context;
	free(ptr);
}

static void *std_realloc(struct alloc_context *context, void *ptr, size_t size)
{
	assert(context == NULL);
	(void) context;
	return realloc(ptr, size);
}

static void *std_alloc_zeroed(struct alloc_context *context, size_t size)
{
	void *allocated = std_alloc(context, size);
	if (allocated) {
		memset(allocated, 0, size);
	}
	return allocated;
}

static const struct alloc_vtable vtable = {
	.alloc = std_alloc,
	.dealloc = std_dealloc,
	.realloc = std_realloc,
	.alloc_zeroed = std_alloc_zeroed,
};

const struct alloc alloc_std = {
	.vtable = &vtable,
	.context = NULL,
};
