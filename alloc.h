#pragma once
#if defined _WIN32 || defined __CYGWIN__
#ifdef BUILDING_ALLOC
#define ALLOC_PUBLIC __declspec(dllexport)
#else
#define ALLOC_PUBLIC __declspec(dllimport)
#endif
#else
#ifdef BUILDING_ALLOC
#define ALLOC_PUBLIC __attribute__((visibility("default")))
#else
#define ALLOC_PUBLIC
#endif
#endif

#include "_alloc_config.h"
#include <stddef.h>

struct alloc_context;

struct alloc_vtable {
	void *(*alloc)(struct alloc_context *context, size_t size);
	void (*dealloc)(struct alloc_context *context, void *ptr);
	void *(*realloc)(struct alloc_context *context, void *ptr, size_t size);
	void *(*alloc_zeroed)(struct alloc_context *context, size_t size);
};

struct alloc {
	const struct alloc_vtable *vtable;
	struct alloc_context *context;
};

static inline void *alloc_alloc(const struct alloc *alloc, size_t size)
{
	return alloc->vtable->alloc(alloc->context, size);
}

static inline void alloc_dealloc(const struct alloc *alloc, void *ptr)
{
	alloc->vtable->dealloc(alloc->context, ptr);
}

static inline void *alloc_realloc(const struct alloc *alloc, void *ptr, size_t size)
{
	return alloc->vtable->realloc(alloc->context, ptr, size);
}

static inline void *alloc_alloc_zeroed(const struct alloc *alloc, size_t size)
{
	return alloc->vtable->alloc_zeroed(alloc->context, size);
}

#ifdef ALLOC_STD
ALLOC_PUBLIC
extern const struct alloc alloc_std;
#endif
